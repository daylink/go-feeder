package main

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/omeid/uconfig"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"golang.org/x/sync/errgroup"
	"gopkg.in/yaml.v2"
)

var (
	ErrTermSig = errors.New("termination error")
)

func main() {
	// setup logger, configs and context
	zapConfig := zap.NewDevelopmentConfig()
	zapConfig.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder

	logger, _ := zapConfig.Build()
	defer logger.Sync()

	ctx := context.Background()

	conf := &Config{}
	c, err := uconfig.Classic(conf, uconfig.Files{
		{"config.yaml", yaml.Unmarshal},
	})

	if err != nil {
		c.Usage()
		os.Exit(1)
	}

	// init Feed Condumer
	feeder := NewFeedConsumer(logger, conf.Feeds)

	// setup http server with Feed Consumer
	hs := &http.Server{Addr: conf.Port, Handler: goServer(feeder, logger)}

	g, ctx := errgroup.WithContext(ctx)

	// start http server
	g.Go(func() error {
		logger.Info("Starting WEB server ")
		if err := hs.ListenAndServe(); err != http.ErrServerClosed {
			return err
		}
		return nil
	})

	// graceful shutdown
	g.Go(func() error {
		sigc := make(chan os.Signal, 1)
		signal.Notify(sigc, syscall.SIGINT, os.Interrupt, syscall.SIGTERM)

		select {
		case <-sigc:
			logger.Info("termination signal caught")
			hs.Shutdown(ctx)
			feeder.Stop()
			logger.Info("Server successfully stopped")
			return ErrTermSig
		case <-ctx.Done():
			return ctx.Err()
		}
	})

	if err := g.Wait(); err != nil && err != ErrTermSig {
		logger.Fatal(
			"failed to wait goroutine group",
			zap.Error(err))
	}
}
