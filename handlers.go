package main

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"go.uber.org/zap"
	"net/http"
)

type Handler struct {
	logger *zap.Logger
	feed   *FeedConsumer
}

func (h Handler) GetAllFeeds(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Max-Age", "300")
	w.Header().Set("Content-Type", "application/json")

	// Json Encode Feeds from service
	b, err := json.Marshal(h.feed.GetFeeds())
	if err != nil {
		h.logger.Error("Json parse error", zap.Error(err))
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Write json to response
	_, err = w.Write(b)
	if err != nil {
		h.logger.Error("Response Write Error", zap.Error(err))
		w.WriteHeader(http.StatusBadRequest)
	}
}

func (h Handler) GetFeed(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Max-Age", "300")
	w.Header().Set("Content-Type", "application/json")

	// Get param from url
	title := chi.URLParam(r, "feedHost")

	// Get feed from service
	feed, ok := h.feed.GetFeeds()[title]
	if !ok {
		http.Error(w, http.StatusText(404), 404)
		return
	}

	// Json Encode Feed from service
	b, err := json.Marshal(feed)
	if err != nil {
		h.logger.Error("Json parse error", zap.Error(err))
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Write json to response
	_, err = w.Write(b)
	if err != nil {
		h.logger.Error("Response Write Error", zap.Error(err))
		w.WriteHeader(http.StatusBadRequest)
	}
}