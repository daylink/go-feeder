FROM golang:1.20-bullseye as builder

WORKDIR /app
COPY go.mod .
COPY go.sum .

RUN git config --global url."https://gitlab-ci-token:glpat-z-NGHxcktfMvq5B_Vsqw@gitlab.com/".insteadOf "https://gitlab.com/"
RUN go mod download
COPY . .
WORKDIR /app
RUN go build -ldflags '-w -s' -o ./out .


FROM golang:1.20-bullseye

WORKDIR /app

COPY --from=builder /app/out ./
WORKDIR /app/

EXPOSE 8080

ENTRYPOINT [ "./out" ]
