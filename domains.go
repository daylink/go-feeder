package main

type Feed struct {
	Title         string      `json:"title,omitempty"`
	Link          string      `json:"link,omitempty"`
	LastBuildDate string      `json:"lastBuildDate,omitempty"`
	Items         []*FeedItem `json:"items"`
}

type FeedItem struct {
	Title string `json:"title,omitempty"`
	Date  string `json:"date,omitempty"`
	Image string `json:"image,omitempty"`
	Link  string `json:"link,omitempty"`
}

// Config for Doc File
type Config struct {
	Feeds map[string]string
	Port  string
}