package main

import (
	"github.com/mmcdole/gofeed"
	"go.uber.org/zap"
	"sync"
	"time"
)

type FeedConsumer struct {
	lock   sync.RWMutex
	logger *zap.Logger
	sites  map[string]string
	fp     *gofeed.Parser
	feeds  map[string]*Feed
	quit   chan bool
}

func NewFeedConsumer(logger *zap.Logger, sites map[string]string) *FeedConsumer {
	f := &FeedConsumer{
		sites:  sites,
		logger: logger,
		fp:     gofeed.NewParser(),
		feeds:  make(map[string]*Feed),
		quit:   make(chan bool),
	}

	// Load Feed and run daemon for it every 5 minute
	f.LoadFeeds()
	go f.daemon(time.Minute * 5)

	return f
}

func (f *FeedConsumer) LoadFeeds()  {
	f.lock.RLock()
	defer f.lock.RUnlock()

	// iterate over all sites in Config and run Feed Parser
	for title, site := range f.sites {
		// parse feed and get items
		feed, err := f.fp.ParseURL(site)
		if err != nil {
			f.logger.Error("get rss feed error", zap.Error(err))
			continue
		}
		// create index for current site and get
		f.feeds[title] = new(Feed)
		f.feeds[title].Title = feed.Title
		f.feeds[title].LastBuildDate = feed.Updated
		f.feeds[title].Link = feed.Link
		for i := 0; i < 3; i++ {
			// handle empty images
			var imgLink string
			if len(feed.Items[i].Enclosures) > 0 {
				imgLink = feed.Items[i].Enclosures[0].URL
			} else {
				imgLink = ""
			}

			fi := &FeedItem{
				Title: feed.Items[i].Title,
				Date: feed.Items[i].Published,
				Image: imgLink,
				Link: feed.Items[i].Link,
			}
			f.feeds[title].Items = append(f.feeds[title].Items, fi)

		}
	}
}

func (f *FeedConsumer) GetFeeds() map[string]*Feed {
	f.lock.RLock()
	defer f.lock.RUnlock()

	return f.feeds
}


// daemon for Update feed every X minute
func (f *FeedConsumer) daemon(tick time.Duration) {

	ticker := time.Tick(tick)

	for {
		select {
		case <-ticker:
			f.LoadFeeds()
		case <-f.quit:
			return
		}
	}
}

// Stop daemon goroutine
func (f *FeedConsumer) Stop() {
	f.quit <- true
}


