package main

import (
	"github.com/go-chi/chi"
	"go.uber.org/zap"
	"net/http"
)

type Server struct {
	router *chi.Mux
}

func goServer(feed *FeedConsumer, logger *zap.Logger) *Server {
	h := Handler{logger: logger, feed: feed}
	r := chi.NewRouter()
	r.Get("/get_feeds/", h.GetAllFeeds)
	r.Get("/get_feeds/{feedHost}/", h.GetFeed)

	return &Server{
		router: r,
	}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}
